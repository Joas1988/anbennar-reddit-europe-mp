
country_decisions = {
	
	push_thunderfist_clan = {
		potential = {
			tag = R62
			exists = R63
		}
		allow = {
			is_at_war = no
			is_free_or_tributary_trigger = yes
			NOT = {
				has_country_modifier = hobgoblin_thunderfist_shamans
			}
			R63 = {
				is_subject_of = ROOT
				is_subject_of_type = slave_state
			}
		}
		effect = {
			country_event = { id = the_command.100 days = 0 }
			open_single_menu = yes	#prevents people from opening the menu multiple times
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	push_bloodsong_clan = {
		potential = {
			tag = R62
			exists = R64
		}
		allow = {
			is_at_war = no
			is_free_or_tributary_trigger = yes
			NOT = {
				has_country_modifier = hobgoblin_bloodsong_advance_guard
			}
			R64 = {
				is_subject_of = ROOT
				is_subject_of_type = slave_state
			}
		}
		effect = {
			country_event = { id = the_command.101 days = 0 }
			open_single_menu = yes	#prevents people from opening the menu multiple times
		}
		ai_will_do = {
			factor = 1
		}
	}

	command_implementing_the_10_reforms = {
		potential = {
			tag = R62
			has_country_flag = command_the_ten_reforms_flag
		}
		
		allow = {
			hidden_trigger = {
				NOT = { has_country_flag = command_the_ten_reforms_menu_open_flag }
			}
		}
	
		effect = {
			country_event = { id = the_command.41 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				always = yes
			} 
		}
	}
}